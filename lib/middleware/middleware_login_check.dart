import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_notification.dart';
import 'package:full_going_app/functions/token_lib.dart';
import 'package:full_going_app/pages/member/page_login.dart';
import 'package:full_going_app/pages/page_home.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? memberToken = await TokenLib.getMemberToken();

    if (memberToken == null) {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageHome()), (route) => false);
      ComponentNotification(
        success: true,
        title: '로그인 성공',
        subTitle: '로그인에 성공하였습니다.',
      ).call();
    }
  }
}
