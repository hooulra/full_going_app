class MyChargeItem {
  double charge;
  String chargeType;
  String dateReg;
  double paymentAmount;
  double resultMileage;

  MyChargeItem(this.charge, this.chargeType, this.dateReg, this.paymentAmount, this.resultMileage);

  factory MyChargeItem.fromJson(Map<String, dynamic> json) {
    return MyChargeItem(
      json['charge'],
      json['chargeType'],
      json['dateReg'],
      json['paymentAmount'],
      json['resultMileage']
    );
  }
}
