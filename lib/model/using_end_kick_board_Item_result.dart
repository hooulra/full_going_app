import 'package:full_going_app/model/using_end_kick_board_item.dart';

class UsingEndKickBoardItemResult {
  int code;
  bool isSuccess;
  String msg;
  UsingEndKickBoardItem data;

  UsingEndKickBoardItemResult(this.code, this.isSuccess, this.msg, this.data);

  factory UsingEndKickBoardItemResult.fromJson(Map<String, dynamic> json) {
    return UsingEndKickBoardItemResult(
      json['code'],
      json['isSuccess'],
      json['msg'],
      UsingEndKickBoardItem.fromJson(json['data']),
    );
  }
}