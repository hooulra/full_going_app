import 'package:full_going_app/model/near_kick_board_item.dart';

class NearKickBoardListResult {
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<NearKickBoardItem>? list;

  NearKickBoardListResult(this.totalItemCount, this.totalPage, this.currentPage, {this.list});

  factory NearKickBoardListResult.fromJson(Map<String, dynamic> json) {
    return NearKickBoardListResult(
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => NearKickBoardItem.fromJson(e)).toList() : [],
    );
  }
}