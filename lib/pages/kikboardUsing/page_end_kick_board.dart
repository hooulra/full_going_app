import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:full_going_app/components/common/component_margin_vertical.dart';
import 'package:full_going_app/components/common/component_text_btn.dart';
import 'package:full_going_app/components/component_divider.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_form_formatter.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/config/config_style.dart';
import 'package:full_going_app/enums/enum_size.dart';
import 'package:full_going_app/model/using_end_kick_board_Item_result.dart';
import 'package:full_going_app/pages/page_home.dart';
import 'package:full_going_app/repository/repo_kickboard_history.dart';

class PageEndKickBoard extends StatefulWidget {
  const PageEndKickBoard({super.key, required this.historyId});

  final int historyId;

  @override
  State<PageEndKickBoard> createState() => _PageEndKickBoardState();
}

class _PageEndKickBoardState extends State<PageEndKickBoard> {
  String dateEnd = "";
  String dateStart = "";
  String kickBoardName = "";
  num resultPass = 0;
  num resultPrice = 0;




  Future<void> _loadKickBoardHistory() async {

    UsingEndKickBoardItemResult result =
    await RepoKickboardHistory().getUsingEndKickBoard(widget.historyId);

    setState(() {
      dateEnd = result.data.dateEnd;
      dateStart = result.data.dateStart;
      kickBoardName = result.data.kickBoardName;
      resultPass = result.data.resultPass;
      resultPrice = result.data.resultPrice;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadKickBoardHistory();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: "킥보드 종료",
      ),
      body: SingleChildScrollView(
          child: Container(
        padding: bodyPaddingLeftRight,
        child: Column(
          children: [
            ComponentMarginVertical(enumSize: EnumSize.bigger),
            Center(
              child:
                  Text("킥보드 사용 내역", style: TextStyle(fontSize: fontSizeSuper)),
            ),
            ComponentMarginVertical(enumSize: EnumSize.bigger),
            ComponentDivider(),
            ComponentMarginVertical(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("사용 시작 시간",
                          style: TextStyle(
                              fontSize: fontSizeMid, color: colorPrimary)),
                      ComponentMarginVertical(),
                      Text("사용 종료 시간", style: TextStyle(fontSize: fontSizeMid)),
                      ComponentMarginVertical(),
                      Text("킥보드 모델명",
                          style: TextStyle(
                              fontSize: fontSizeMid, color: colorPrimary)),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(dateStart,
                          style: TextStyle(
                              fontSize: fontSizeMid, color: colorRed)),
                      ComponentMarginVertical(),
                      Text(dateEnd, style: TextStyle(fontSize: fontSizeMid)),
                      ComponentMarginVertical(),
                      Text(kickBoardName,
                          style: TextStyle(
                              fontSize: fontSizeMid, color: colorRed)),
                    ],
                  ),
                ),
              ],
            ),
            ComponentMarginVertical(),
            ComponentDivider(),
            ComponentMarginVertical(enumSize: EnumSize.bigger),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 50),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("사용 패스 시간",
                          style: TextStyle(
                              fontSize: fontSizeBig, color: colorPrimary)),
                      ComponentMarginVertical(),
                      Text("결제 금액",
                          style: TextStyle(
                              fontSize: fontSizeBig, color: colorPrimary)),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(right: 50),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(maskWonFormatter.format(resultPass) + " 원",
                          style: TextStyle(fontSize: fontSizeBig)),
                      ComponentMarginVertical(),
                      Text(maskWonFormatter.format(resultPrice) + " 원",
                          style: TextStyle(fontSize: fontSizeBig)),
                    ],
                  ),
                ),
              ],
            ),
            ComponentMarginVertical(enumSize: EnumSize.bigger),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                  text: '확인',
                  callback: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) => const PageHome()),
                            (route) => false);
                  }),
            ),
          ],
        ),
      )),
    );
  }
}
