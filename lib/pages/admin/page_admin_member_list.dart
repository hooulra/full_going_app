import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:full_going_app/config/config_style.dart';

class PageAdminMemberList extends StatefulWidget {
  const PageAdminMemberList({super.key});

  @override
  State<PageAdminMemberList> createState() => _PageAdminMemberListState();
}

class _PageAdminMemberListState extends State<PageAdminMemberList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: const ComponentAppbarPopup(
            title: '회원 관리'
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: bodyPaddingLeftRight,
          ),
        ));
  }
}
