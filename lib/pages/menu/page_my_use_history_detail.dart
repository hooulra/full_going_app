import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:full_going_app/components/common/component_text_btn.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_form_formatter.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/config/config_style.dart';
import 'package:full_going_app/model/my_history_detail_item_result.dart';
import 'package:full_going_app/repository/repo_kickboard_history.dart';

class PageMyUseHistoryDetail extends StatefulWidget {
  const PageMyUseHistoryDetail({super.key, required this.historyId});

  final int historyId;

  @override
  State<PageMyUseHistoryDetail> createState() => _PageMyUseHistoryDetailState();
}

class _PageMyUseHistoryDetailState extends State<PageMyUseHistoryDetail> {
  String dateEnd = "";
  String dateStart = "";
  String kickBoardName = "";
  num resultPass = 0;
  num resultPrice = 0;

  Future<void> _loadProfileDetailData() async {
    MyHistoryDetailItemResult result =
        await RepoKickboardHistory().getDetailHistory(widget.historyId);

    setState(() {
      dateEnd = result.data.dateEnd;
      dateStart = result.data.dateStart;
      kickBoardName = result.data.kickBoardName;
      resultPass = result.data.resultPass;
      resultPrice = result.data.resultPrice;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadProfileDetailData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '킥보드 이용 상세내역',
      ),
      body: Padding(
        padding: bodyPaddingLeftRight,
        child: Column(
          children: [
            Spacer(flex: 1),
            Text("킥보드 이용 내역",
                style: TextStyle(
                    fontSize: fontSizeSuper, fontWeight: FontWeight.bold)),
            Spacer(flex: 2),
            _buildInfoRow("킥보드 모델명", kickBoardName),
            Spacer(flex: 1),
            _buildInfoRow("킥보드 사용 시작 시간", dateStart),
            Spacer(flex: 1),
            _buildInfoRow("킥보드 사용 종료 시간", dateEnd),
            Spacer(flex: 2),
            _buildInfoRow("사용 패스 시간", "${resultPass} 분", isHighlight: true),
            Spacer(flex: 1),
            _buildInfoRow("결제 금액", maskWonFormatter.format(resultPrice) + " 원",
                isHighlight: true),
            Spacer(flex: 2),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                iconData: Icons.check_circle,
                text: "확인",
                textColor: Colors.white,
                borderColor: colorPrimary,
                bgColor: colorPrimary,
                callback: () {
                  Navigator.pop(context);
                },
              ),
            ),
            Spacer(flex: 2),
          ],
        ),
      ),
    );
  }

  Widget _buildInfoRow(String title, String value, {bool isHighlight = false}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(title,
            style: TextStyle(
                fontSize: 16,
                color: isHighlight ? colorPrimary : colorDarkGray)),
        Text(value,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
      ],
    );
  }
}
