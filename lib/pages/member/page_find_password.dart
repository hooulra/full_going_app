import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:full_going_app/components/common/component_margin_vertical.dart';
import 'package:full_going_app/components/common/component_notification.dart';
import 'package:full_going_app/components/common/component_text_btn.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_form_formatter.dart';
import 'package:full_going_app/config/config_form_validator.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/config/config_style.dart';
import 'package:full_going_app/enums/enum_size.dart';
import 'package:full_going_app/model/member_password_find_request.dart';
import 'package:full_going_app/pages/member/page_login.dart';
import 'package:full_going_app/styles/style_form_decoration_full_going.dart';

import '../../components/common/component_custom_loading.dart';
import '../../repository/repo_member.dart';

class PageFindPassword extends StatefulWidget {
  const PageFindPassword({super.key});

  @override
  State<PageFindPassword> createState() => _PageFindPassword();
}

class _PageFindPassword extends State<PageFindPassword> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doPasswordFind(
      MemberPasswordFindRequest memberPasswordFindRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().doPasswordFind(memberPasswordFindRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '임시 비밀번호 발급 완료',
        subTitle: '임시 비밀번호로 변경 되었습니다.',
      ).call();

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageLogin()),
          (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '임시 비밀번호 발급 실패',
        subTitle: '임시 비밀번호 발급이 실패했습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "비밀번호 찾기"),
      body: SingleChildScrollView(
        child: FormBuilder(
          autovalidateMode: AutovalidateMode.disabled,
          key: _formKey,
          child: Container(
            padding: bodyPaddingLeftRight,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              const ComponentMarginVertical(enumSize: EnumSize.big),
              Text("아래 회원 정보를 입력하시면", style: TextStyle(fontSize: fontSizeBig)),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("문자로 임시 비밀번호를 발송",
                      style: TextStyle(fontSize: fontSizeBig, color: colorRed)),
                  Text("해드립니다.", style: TextStyle(fontSize: fontSizeBig)),
                ],
              ),
              const ComponentMarginVertical(enumSize: EnumSize.big),
              FormBuilderTextField(
                keyboardType: TextInputType.text,
                name: 'username',
                decoration:
                    StyleFormDecorationOfFullGoing().getInputDecoration("아이디"),
                validator: validatorOfLoginUsername,
              ),
              const ComponentMarginVertical(),
              FormBuilderTextField(
                name: 'phoneNumber',
                keyboardType: TextInputType.number,
                inputFormatters: [
                  maskPhoneNumberFormatter
                  //13자리만 입력받도록 하이픈 2개+숫자 11개
                ],
                decoration: StyleFormDecorationOfFullGoing().getInputDecoration(
                  "연락처",
                  useHintText: true,
                  hintText: "XXX-XXXX-XXXX",
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),
              const ComponentMarginVertical(enumSize: EnumSize.bigger),
              Container(
                width: MediaQuery.sizeOf(context).width,
                child: ComponentTextBtn(
                  text: "임시 비밀번호 받기",
                  bgColor: colorPrimary,
                  textColor: Colors.white,
                  callback: () {
                    if (_formKey.currentState!.saveAndValidate()) {
                      MemberPasswordFindRequest memberPasswordFindRequest =
                          MemberPasswordFindRequest(
                        _formKey.currentState!.fields['username']!.value,
                        _formKey.currentState!.fields['phoneNumber']!.value,
                      );
                      _doPasswordFind(memberPasswordFindRequest);
                    }
                  },
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
