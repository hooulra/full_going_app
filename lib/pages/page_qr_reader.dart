import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:full_going_app/components/common/component_custom_loading.dart';
import 'package:full_going_app/components/common/component_notification.dart';
import 'package:full_going_app/components/common/component_text_btn.dart';
import 'package:full_going_app/config/config_style.dart';
import 'package:full_going_app/model/kick_board_start_request.dart';
import 'package:full_going_app/pages/kikboardUsing/page_using_kick_board.dart';
import 'package:full_going_app/repository/repo_kickboard.dart';
import 'package:qr_code_dart_scan/qr_code_dart_scan.dart';

class PageQrReader extends StatefulWidget {
  const PageQrReader({super.key, required this.posX, required this.posY});

  final double posX;
  final double posY;

  @override
  State<PageQrReader> createState() => _PageQrReaderState();
}

class _PageQrReaderState extends State<PageQrReader> {
  Result? currentResult;

  Future<void> _setStart(
      int kickboardId, KickBoardStartRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoKickboard().setStart(kickboardId, request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '사용 시작 완료',
        subTitle: '킥보드 사용이 시작 되었습니다.',
      ).call();

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageUsingKickBoard()),
              (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '사용 시작 실패',
        subTitle: '킥보드 사용이 실패 했습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: '킥보드 QR코드 스캔'),
      body: _buildBody(),
    );
  }


  Widget _buildBody() {
    if (currentResult?.text != null) {
      return Container(
        padding: bodyPaddingLeftRight,
        child: ComponentTextBtn(
          text: '사용 시작',
          callback: () {
            KickBoardStartRequest kickBoardStartRequest = KickBoardStartRequest(widget.posX, widget.posY);
            _setStart(int.parse(currentResult.toString()), kickBoardStartRequest);

            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) => PageUsingKickBoard()),
            );
          },
        ),
      );
    } else {
      return QRCodeDartScanView(
        scanInvertedQRCode: true,
        onCapture: (Result result) {
          setState(() {
            currentResult = result;
          });
        },
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            margin: EdgeInsets.all(20),
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(currentResult?.text ?? '킥보드 상단 QR코드를 찍으세요'),
              ],
            ),
          ),
        ),
      );
    }
  }
}
